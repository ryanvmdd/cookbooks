node[:deploy].each do |app_name, deploy_config|
	# determine root folder of new app deployment
  	app_root = "#{deploy_config[:deploy_to]}/current"
	
	execute 'clean_cache' do
		user 'www-data'
		group 'www-data'
		cwd app_root
		command "php bin/console cache:clear --env=$ENV"
		action :run
	end

	execute 'cache_warmup' do
		user 'www-data'
		group 'www-data'
		cwd app_root
		command "php bin/console cache:warmup --env=$ENV --no-debug"
		action :run
	end

	execute 'queue_command' do
		user 'www-data'
		group 'www-data'
		cwd app_root
		command "php bin/console app:queue --env=$ENV --no-debug"
		action :run
	end
end
