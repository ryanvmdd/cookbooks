cron_env = { "env" => "prod", "SYMFONY__SECRET" => "Mandrigyn68" }

cron "inventory_runner" do
	hour "*"
	minute "5"
	user "www-data"
	command "php /srv/www/webapp/bin/console app:inventory-runner --env=$env"
end

cron "reader_runner" do
    hour "*"
    minute "5"
    user "www-data"
    command "php /srv/www/webapp/bin/console app:reader:report --env=$env"
end
