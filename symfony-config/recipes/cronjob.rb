cron "inventory_import" do
	minute	'0'
	hour    '*'
	day     '*'
    	month   '*'
	command "php /srv/www/mddwebapp/current/bin/console app:inventory-runner"
end

cron "reader_report" do
    minute  '0'
    hour    '*'
    day     '*'
    month   '*'
    command "php /src/www/mddwebapp/current/bin/console app:reader:report"
end

cron "alert" do
	minute	'*/10'
	hour   	'*'
	day	'*'
	month	'*'
	command "php /srv/www/mddwebapp/current/bin/console app:message-publish-runner > /home/ubuntu/logs/messages.log 2>&1"
end
