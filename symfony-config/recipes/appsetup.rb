node[:deploy].each do |app_name, deploy_config|
	# determine root folder of new app deployment
  	app_root = "#{deploy_config[:deploy_to]}/current"

	directory app_root + "/var" do
		owner 'deploy'
		group 'www-data'
		mode '775'
		recursive true
		action :create
	end

	directory app_root + "/var/cache" do 
		owner 'deploy'
		group 'www-data'
		mode '775'
		recursive true
		action :create
	end

	directory app_root + "/var/logs" do
		owner 'deploy'
		group 'www-data'
		mode '775'
		recursive true
		action :create
	end
end
